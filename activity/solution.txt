[1] Creating "enrollment_db" database
CREATE DATABASE enrollment_db;


[2] Creating "students" table
CREATE TABLE students(
	student_id INT NOT NULL AUTO_INCREMENT,
    student_name VARCHAR(50) NOT NULL,
    PRIMARY KEY(student_id)
);


[3] Creating "teachers" table
CREATE TABLE teachers(
	teacher_id INT NOT NULL AUTO_INCREMENT,
    teacher_name VARCHAR(50) NOT NULL,
    PRIMARY KEY(teacher_id)
);


[4] Creating "courses" table
CREATE TABLE courses(
	course_id INT NOT NULL AUTO_INCREMENT,
	teacher_id INT NOT NULL,
	course_name VARCHAR(50) NOT NULL,
	PRIMARY KEY(course_id),
	CONSTRAINT fk_courses_teachers_id
	FOREIGN KEY (teacher_id) REFERENCES teachers(teacher_id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);


[5] Creating "student_courses" table
CREATE TABLE student_courses(
	course_id INT NOT NULL,
	student_id INT NOT NULL,
	CONSTRAINT fk_studentCourses_courses_id
	FOREIGN KEY (course_id) REFERENCES courses(course_id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	CONSTRAINT fk_studentCourses_students_id
	FOREIGN KEY (student_id) REFERENCES students(student_id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);